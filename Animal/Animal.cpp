﻿#include <iostream>

using namespace std;

class Animal
{
public:
    Animal()
    {}
    
    virtual void voice() 
    {
        cout << "Text" << endl;
    }
};

class Dogs:public Animal
{
public:
    Dogs()
    {}
    
    void voice() override
    {
        cout << "Dog say \"Woof\"." << endl;
    }
};

class Cats:public Animal
{
public:
    Cats()
    {}
    
    void voice() override
    {
        cout << "Cat say \"Meow\"." << endl;
    }
};

class Cows:public Animal
{
public:
    Cows()
    {}
    
    void voice() override
    {
        cout << "Cow say \"Moo\"." << endl;
    }
};

int main()
{
    std::cout << "Hello World!\n";
   
    Dogs Dog1;
    Cats Cat1;
    Cows Cow1;
    Animal* Anim[3] = { &Dog1, &Cat1, &Cow1 };

    for (int i = 0; i < 3; i++) Anim[i]->voice();

    return 0;
}

